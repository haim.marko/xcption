#!/usr/bin/python3

import requests
import logging
import csv
import json
import sys
import getpass
import datetime
import argparse
from urllib.parse import urlparse
from prettytable import PrettyTable

# Disable warnings for insecure connections
requests.packages.urllib3.disable_warnings(requests.packages.urllib3.exceptions.InsecureRequestWarning)

# Function to get exports
def get_isilon_info(endpoint: str, username: str, password: str) -> dict:

    response = requests.get(endpoint, auth=(username, password), verify=False)
    if not response.ok:
        logging.error(f'could not get exports: {response.text}')
        exit(1)
    return response.json()

def output_isilon_exports(fsexports, type):
    #build the table object
    if type == 'table':
        table = PrettyTable()
        table.field_names = ['FileServer','Path', 'Root Clients', 'Read Clients', 'Write Clients']
    elif type == 'csv':
        writer = csv.writer(sys.stdout,delimiter=",")
        writer.writerow(['FileServer','Path', 'Root Clients', 'Read Clients', 'Write Clients'])

    for fs in fsexports:
        fileserver = fs['fileserver']
        exports = fs['info']
        for export in exports['exports']:
            path = export['paths'][0] if export['paths'] else ''
            try:
                root_clients = ':'.join(export.get('root_clients', []))
            except:
                root_clients = ''
            try:
                read_clients = ':'.join(export.get('read_only_clients', []))
            except:
                read_clients = ''
            try:
                write_clients = ':'.join(export.get('read_write_clients', []))
            except:
                write_clients = ''
            if type == 'table':
                table.add_row([fileserver,path, root_clients.replace(":","\n"), read_clients.replace(":","\n"), write_clients.replace(":","\n")])
            elif type == 'csv':
                writer.writerow([fileserver,path, root_clients, read_clients, write_clients])

    if type == 'table':
        table.border = False
        table.align = 'l'
        print(table)
    elif type == 'json':
        print(json.dumps(fsexports, indent=4))

def output_isilon_shares(fsshares, type):
    #build the table object
    if type == 'table':
        table = PrettyTable()
        table.field_names = ['FileServer','Name', 'Path', 'Comment','ABE', 'OPlocks', 'Change Notify','ACL Name','ACL Type','ACL Permission','Root ACL','First']
    elif type == 'csv':
        writer = csv.writer(sys.stdout,delimiter=",")
        writer.writerow(['FileServer','Name', 'Path', 'Comment', 'ABE', 'OPlocks', 'Change Notify','ACL Name','ACL Type','ACL Permission','Root ACL','First'])

    for fs in fsshares:
        fileserver = fs['fileserver']
        shares = fs['info']
        for share in shares['shares']:
            name = share.get('name', '')
            path = share.get('path', '')
            comment = share.get('description', '')

            abe = share.get('access_based_enumeration', '')
            abe = 'TRUE' if abe else 'FALSE'

            oplocks = share.get('oplocks', '')
            oplocks = 'FALSE' if oplocks else 'FALSE'

            change_notify = share.get('change_notify', '')
            change_notify = 'TRUE' if change_notify else 'FALSE'

            permissions = share.get('permissions', [])

            share_acl = []
            for permission in permissions:
                ppermission = permission.get('permission', '')
                if ppermission == 'full': ppermission = 'full_control'
                ptype = permission.get('permission_type', '')
                ptrustee = permission.get('trustee', {})
                pname = ptrustee.get('name', '')

                share_acl.append(f'{pname}^{ptype}^{ppermission}')

            root_acl = []
            root_permissions = share.get('run_as_root', [])
            for permission in root_permissions:
                root_acl.append(permission.get('name', ''))

            max_len = max(len(share_acl), len(root_acl))

            for i in range(0, max_len):
                sname = ''; stype=''; spermission=''
                rname = ''
                first = 'FALSE'
                if i == 0: first = 'TRUE'

                if i<len(share_acl):
                    sname, stype, spermission = share_acl[i].split('^')
                if i<len(root_acl):
                    rname = root_acl[i]

                if type == 'table':
                    table.add_row([fileserver,name,path,comment,abe,oplocks,change_notify,sname,stype,spermission,rname,first])
                elif type == 'csv':
                    writer.writerow([fileserver,name,path,comment,abe,oplocks,change_notify,sname,stype,spermission,rname,first])

        if type == 'table':
            table.border = False
            table.align = 'l'
            print(table)
        elif type == 'json':
            print(json.dumps(fsshares, indent=4))

# Main function
def main():
    #initialize ars parse
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--nas-type',help="nas type: [isilon|ontap]",choices=['isilon','ontap'],required=False,default='ontap',type=str)
    parser.add_argument('-r', '--required-info',help="required information: [exports|shares] ",choices=['exports','shares'],required=False,default='ontap',type=str)
    parser.add_argument('-e', '--endpoint', help='rest endpoint (ex: https://nas:443)', required=True, type=str)
    parser.add_argument('-f', '--fileservers', help='comma seperated list of the fileservers (ex. OnTap SVM, Isilon Zone,..) if required', required=False, type=str)
    parser.add_argument('-u', '--username', help='username', required=True, type=str)
    parser.add_argument('-p', '--password', help='password', required=False, type=str)
    parser.add_argument('-o', '--output',help="output type: [table|csv|json]",choices=['table','csv','json'],default='table',required=False,type=str)
    parser.add_argument('-x', '--xcption-dir', help='xcption install dir, will be used to translate paths into destination',default='',required=False, type=str)
    parser.add_argument('-v', '--verbose-logging', help='debug logging', default=False, action='store_true')
    
    args = parser.parse_args()

    #initialize logging
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    formatterdebug = logging.Formatter('%(asctime)s - %(levelname)s - %(funcName)s - %(message)s')

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    if args.verbose_logging: ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    log.addHandler(ch)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)

    #used for directory names
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

    # validate args.endpoint is in url format
    if not urlparse(args.endpoint).scheme:
        log.error(f'Invalid endpoint: {args.endpoint}')
        exit(1)

    if not args.password:
        #input password if not provided
        password = getpass.getpass()
    
    if args.xcption_dir:
        xcption_job_file = f'{args.xcption_dir}/system/xcp_repo/jobs/jobs.json'
        if os.path.exists(xcption_job_file):
            try:
                logging.debug("loading existing json file:"+xcption_job_file)
                with open(xcption_job_file) as f:
                    xcption_jobs = json.load(f)
            except Exception as e:
                logging.error("could not load xcption job file:"+xcption_job_file)
                exit(1)
            
            for job in xcption_jobs:
                for src in job.keys():
                    dst = job[src]['dst']



    if args.nas_type == 'isilon' and args.required_info == 'exports':
        output = []
        for fileserver in args.fileservers.split(','):
                endpoint = f"{args.endpoint}/platform/4/protocols/nfs/exports{'?zone='+fileserver if fileserver else ''}"
                output.append({"fileserver": fileserver, "info": get_isilon_info(endpoint=endpoint, username=args.username, password=args.password)})
        output_isilon_exports(output,args.output)
        endpoint = f"{args.endpoint}/platform/4/protocols/nfs/exports{'?zone='+args.fileserver if args.fileserver else ''}"
        output = get_isilon_info(endpoint=endpoint, username=args.username, password=args.password)
        output_isilon_exports(output,args.output)

    if args.nas_type == 'isilon' and args.required_info == 'shares':
        output = []
        for fileserver in args.fileservers.split(','):    
            endpoint = f"{args.endpoint}/platform/4/protocols/smb/shares{'?zone='+fileserver if fileserver else ''}"
            output.append({"fileserver": fileserver, "info": get_isilon_info(endpoint=endpoint, username=args.username, password=args.password)})
        output_isilon_shares(output,args.output)

if __name__ == '__main__':
    main()
