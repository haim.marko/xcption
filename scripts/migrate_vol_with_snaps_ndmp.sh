#!/usr/bin/bash
export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

export SRC_CLUSTER_SVM=admin@cluster1:/svm1_cluster1
export DST_CLUSTER_SVM=admin@cluster1:/svm2_cluster1

export LOG_DIR=/var/log/migrate_vol_with_snaps_ndmp
mkdir -p ${LOG_DIR}

export MAIL_TO=test@test.com 


if [ $# -ne 1 ]; then
    echo "Usage: $0 <vol>"
    exit 1
fi

export VOL=$1


${SCRIPT_DIR}/migrate_vol_with_snaps_ndmp.py -s ${SRC_CLUSTER_SVM}/${VOL} -d ${DST_CLUSTER_SVM}/${VOL} -m 2>&1 | tee -a ${LOG_DIR}/${VOL}.log

mail -s "Migrate ${VOL} with snaps NDMP" -a ${LOG_DIR}/${VOL}.log ${MAIL_TO}
echo "Mail sent to ${MAIL_TO}"
