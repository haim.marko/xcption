#!/usr/bin/python3

import json, os, re, csv, argparse, sys, logging, subprocess, datetime, time

#run ssh to remote host
def ssh (hostname:str, cmd: list = []):
    cmdarr =  ['ssh','-oStrictHostKeyChecking=no','-oBatchMode=yes',hostname] + cmd

    logging.debug ("ssh command: ssh "+hostname+" "+" ".join(cmd))

    result = subprocess.run(cmdarr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = {'returncode': result.returncode,
              'stdout': result.stdout.decode('utf-8'),
              'stderr': result.stderr.decode('utf-8'),
              'stdoutlines': result.stdout.decode('utf-8').splitlines()
             }   
    matchObj = re.search(r" jobId \'(\d+)\'",output['stdout'])
    if matchObj:
        output['jobid'] = matchObj.group(1) 

    for line in output['stdoutlines']:
        if line.startswith("ERROR:"):
            output['error'] = line

    return(output) 

#validate ontap ndmp 
def validate_ontap_ndmp(ontappath):
    matchObj = re.match(r"^([a-zA-Z0-9\._%+-_]+)@([a-zA-Z0-9-_]+):\/([a-zA-Z0-9-_]+)\/([a-zA-Z0-9_]+)(.*)$",ontappath)
    if matchObj:
        ontapuser = matchObj.group(1)
        ontaphost = matchObj.group(2)
        svm = matchObj.group(3)
        vol = matchObj.group(4)
        dir = matchObj.group(5)
        

    if not matchObj or (dir and not dir.startswith('/')):
        logging.error("invalid ontap ndmp path:"+ontappath+" should be in the following format: user@cluster:/svm/vol[/dir]")
        exit(1)    
    
    out = ssh(ontapuser+'@'+ontaphost,['node','run','-node','*','-command','version'])    
    if out['returncode']:
        if 'is not a recognized command' in out['stdout']:
            # could not run node run command - will not work if using vserver level login 
            logging.error("could not validtae:" + ontapuser+'@'+ontaphost+" is ontap cluster level login")
        else:
            logging.error("could not connect to:" + ontapuser+'@'+ontaphost+ " using SSH: "+out['stderr']+' '+out['stdout'])
        exit(1)    
    
    out = ssh(ontapuser+'@'+ontaphost,"network interface show -role cluster-mgmt -instance".split(" "))
    if out['returncode']:
        logging.error("could not identify ontap cluste-mgmt lif")
        exit(1)

    matchObj = re.search(r"Vserver Name:\s([a-zA-Z0-9_-]+)",out['stdout'])
    if matchObj:
        clustername = matchObj.group(1)
    else:
        logging.error("could not identify ontap cluster name")
        exit(1)        


    out = ssh(ontapuser+'@'+ontaphost,['vserver','services','ndmp','show','-vserver',clustername])
    if out['returncode']:
        logging.error("could not connect to:" + ontapuser+'@'+ontaphost+ " using SSH: "+out['stderr']+' '+out['stdout'])
        exit(1)    

    if 'Enable NDMP on Vserver: false' in out['stdout']:
        logging.warning("NDMP is not active on OnTap SVM: "+svm+" please start it using: vserver services ndmp on -vserver "+clustername)    
    # elif 'Enable NDMP on Vserver: true' in out['stdout']:
    #     logging.info("SSH connectivity and NDMP readiness validated to ontap cluster: "+clustername)    


    ndmpinfo = {}
    ndmpinfo['full_path'] = ontappath
    ndmpinfo['host'] = ontaphost
    ndmpinfo['user'] = ontapuser
    ndmpinfo['path'] = '/'+svm+'/'+vol+dir 
    ndmpinfo['svm'] = svm
    ndmpinfo['vol'] = vol
    ndmpinfo['dir'] = dir

    # get ndmp password 
    cmd = 'vserver services ndmp generate-password -vserver '+clustername+' -user '+ontapuser
    out = ssh(ontapuser+'@'+ontaphost,cmd.split(' '))
    matchObj = re.search(r"Password:\s(\w+)",out['stdout'])
    if matchObj:
        ndmpinfo['ndmppass'] = matchObj.group(1)

    # get volume details  
    cmd = 'volume show -vserver '+svm+' -volume '+vol+' -fields state,junction-path,type,node'
    out = ssh(ontapuser+'@'+ontaphost,cmd.split(' '))
    matchObj = re.search(fr"\s{vol}\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s*",out['stdout'])
    if matchObj:
        ndmpinfo['atate'] = matchObj.group(1)    
        ndmpinfo['junction'] = matchObj.group(2)    
        ndmpinfo['type'] = matchObj.group(3)    
        ndmpinfo['node'] = matchObj.group(4)
    else:
        logging.error(f"could not find ontap volume: {svm}:{vol}")
        exit(1)
    
    if ndmpinfo['atate'] != 'online':
        logging.error(f"ontap volume: {svm}:{vol} is not online")
        exit(1)

    #when volume is mounted validate path exists 
    if ndmpinfo['junction'].startswith('/'):
        cmd = 'vserver security file-directory show -vserver '+svm+' -path'
        out = ssh(ontapuser+'@'+ontaphost,cmd.split(' ')+['"'+ndmpinfo['junction']+dir+'/.'+'"'])
        matchObj = re.search(fr"File Path:\s+{ndmpinfo['junction']}",out['stdout'])
        if not matchObj:
            logging.error(f"ontap path: {svm}:{vol}{dir} does not exists")
            exit(1)
    else:
        logging.warning(f"could not validate path because ontap volume: {svm}:{vol} is not mounted on the svm")    

    #look for intercluster IP address we will use for NDMP on the owning node 
    cmd = 'network interface show -vserver '+clustername+' -role intercluster -curr-node '+ndmpinfo['node']+' -status-admin up -status-oper up -fields curr-node,address'
    out = ssh(ontapuser+'@'+ontaphost,cmd.split(' '))
    matchObj = re.search(fr"\s+([0-9.]+)\s+{ndmpinfo['node']}\s*",out['stdout'])
    if matchObj:
        ndmpinfo['ndmpip'] = matchObj.group(1)
    else:
        logging.error(f"could not find ip address to use for ontap NDMP for: {svm}:{vol}, make sure intercluster lif is avaialble on node:{ndmpinfo['node']}")
        exit(1)        

    ndmpinfo['ndmppath'] = ndmpinfo['ndmpip']+':'+ndmpinfo['path']
    
    #map snapshots in the volume 
    cmd = 'snapshot show -vserver '+svm+' -volume '+vol+' -fields snapshot'
    out = ssh(ontapuser+'@'+ontaphost,cmd.split(' '))
    ndmpinfo['snapshots'] = []
    for line in out['stdoutlines']:
        matchObj = re.search(fr"{svm}\s+{vol}\s+(.+)$",line.rstrip())
        if matchObj:
            snapshot = matchObj.group(1)
            ndmpinfo['snapshots'].append(snapshot)
    
    return(ndmpinfo)

def run_cmd_on_host (cmd: list = []):
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = {'returncode': result.returncode,
              'stdout': result.stdout.decode('utf-8'),
              'stderr': result.stderr.decode('utf-8'),
              'stdoutlines': result.stdout.decode('utf-8').splitlines()
             }
    matchObj = re.search(" jobId \'(\d+)\'",output['stdout'])
    if matchObj:
        output['jobid'] = matchObj.group(1)

    for line in output['stdoutlines']:
        if line.startswith("ERROR:"):
            output['error'] = line

    return(output)


def run_xcption_cmd(cmd: list = []):
    if not os.path.isfile(XCPTION_PATH):
        logging.error("xcption.py not found at: "+XCPTION_PATH)
        exit(1)
    if not os.access(XCPTION_PATH, os.X_OK):
        logging.error("xcption.py is not executable: "+XCPTION_PATH)
        exit(1)
    
    cmd = [XCPTION_PATH] + cmd
    xcption_cmd_output = run_cmd_on_host(cmd)
    if xcption_cmd_output['returncode']:
        logging.warning(f"xcption command failed: {cmd} retrying in 10 seconds")
        time.sleep(10)
        xcption_cmd_output = run_cmd_on_host(cmd)
    return(xcption_cmd_output)

def get_xcption_status(job: str='', src: str=''):
    cmd = ['status','-o','json']
    if job:
        cmd += ['-j',job]
    if src:
        cmd += ['-s',src]

    xcption_status = run_xcption_cmd(cmd)
    if xcption_status['returncode']:
        logging.error(f"could not get xcption status: {xcption_status['stderr']}")
        exit(1)
    try:
        return(json.loads(xcption_status['stdout']))
    except json.JSONDecodeError as e:
        logging.error(f"could not parse xcption status into json: {xcption_status['stdout']}")
        exit(1)

def main():
    #initialize ars parse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--src-path', help='src path in the format og user@cluster:/svm/vol[/path]', required=True, type=str)
    parser.add_argument('-d', '--dst-path', help='dst path in the format og user@cluster:/svm/vol[/path]', required=True, type=str)
    parser.add_argument('-m', '--migrate-snapshots', help='migrate snapshots', required=False, default=False,action='store_true')
    parser.add_argument('-x', '--xcption-path', help='path to xcption.py (default /root/xcption/xcption.py)', required=False, type=str, default='/root/xcption/xcption.py')
    parser.add_argument('-v', '--verbose-logging', help='debug logging', default=False, action='store_true')
    args = parser.parse_args()

    #initialize logging
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    formatterdebug = logging.Formatter('%(asctime)s - %(levelname)s - %(funcName)s - %(message)s')

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    if args.verbose_logging: ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    log.addHandler(ch)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)

    #used for directory names
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

    src = args.src_path
    dst = args.dst_path
    srcInfo = validate_ontap_ndmp(src)
    dstInfo = validate_ontap_ndmp(dst)

    job = srcInfo['vol']

    #set xcption.py path as global
    global XCPTION_PATH 
    XCPTION_PATH = args.xcption_path
    
    #check if xcption is up and running 
    status = run_xcption_cmd(['nodestatus'])
    if (status['returncode']):
        logging.error(f"xcption is not running, make sure it is running on the host using the command: {args.xcption_path} nodestatus")
        exit(1)
                
    if not args.migrate_snapshots or len(srcInfo['snapshots']) == 0:
        foundJob = False
        job_status = get_xcption_status(job=job,src='*'+src)

        try:
            js = job_status[job][src]
            foundJob = True
        except:
            foundJob = False

        try:
            status = js['phases'][-1]['status']
        except:
            status = 'not started'            
        
        #if job found display status
        if foundJob:
            logging.info(f"ndmpcopy job from src:{srcInfo['full_path']} to dst:{dstInfo['full_path']} exists with status: {status}")

        #if job not found create it
        if not foundJob:
            logging.info(f"creating ndmpcopy job from src:{srcInfo['full_path']} to dst:{dstInfo['full_path']}")
            result = run_xcption_cmd(['create','-j',job,'-s',src,'-d',dst,'-t','ndmpcopy','-p','70','-n','0 0 31 2 *'])
            if result['returncode']:
                logging.error(f"could not create job: {result['stderr']}")
                exit(1)
            job_status = get_xcption_status(job=job,src='*'+src)
        
        status = 'not started'
        if foundJob:
            try:
                status = job_status[job][src]['phases'][-1]['status']
            except:
                status = 'not started'
        
        #start baseline if prev job failed or not started
        if status in ['not started']:
            logging.info(f"starting baseline for job: ndmpcopy from src:{src} to dst:{dst}")
            result = run_xcption_cmd(['baseline','-j',job,'-s','*'+src])
            if result['returncode']:
                logging.error(f"could not start job: {result['stderr']}")
                exit(1)
        
            while True:
                time.sleep(8)
                job_status = get_xcption_status(job=job,src='*'+src)[job][src]
                status = job_status['phases'][-1]['status']
                logging.info(f"job status: {status}")

                if status == 'failed':
                    logging.error(f"job failed:")
                    print(job_status['phases'][-1]['stdoutlogcontent'])
                    exit(1)

                if status == 'complete':
                    break

    if args.migrate_snapshots and len(srcInfo['snapshots']) > 0:        
        #enable creation of duplicate destination
        os.environ['XCPTION_ALLOW_DUPLICATE_DST'] = 'TRUE'
        #allow sync without baseline
        os.environ['XCPTION_FORCE_SYNC'] = 'TRUE'
        
        #get status 
        job_status = get_xcption_status(job=job)
        
        #create job for each snapshot if not exisiting
        for snapshot in srcInfo['snapshots']:
            src_path_with_snapshot = f"{srcInfo['user']}@{srcInfo['host']}:/{srcInfo['svm']}/{srcInfo['vol']}/.snapshot/{snapshot}{srcInfo['dir']}"
            
            try:
                js = job_status[job][src_path_with_snapshot]
                foundJob = True
            except:
                foundJob = False

            try:
                status = js['phases'][-1]['status']
            except:
                status = 'not started'

            #if job found display status
            if foundJob:
                logging.info(f"ndmpcopy job from src:{src_path_with_snapshot} to dst:{dstInfo['full_path']} exists with status: {status}")

            #if job not found create it
            if not foundJob:
                logging.info(f"creating ndmpcopy job from src:{src_path_with_snapshot} to dst:{dstInfo['full_path']}")
                result = run_xcption_cmd(['create','-j',job,'-s',src_path_with_snapshot,'-d',dst,'-t','ndmpcopy','-p','70','-n','0 0 31 2 *'])
                if result['returncode']:
                    logging.error(f"could not create job: {result['stderr']}")
                    exit(1)

        snapcount = 0
        #start job for each snapshot
        for snapshot in srcInfo['snapshots']:
            src_path_with_snapshot = f"{srcInfo['user']}@{srcInfo['host']}:/{srcInfo['svm']}/{srcInfo['vol']}/.snapshot/{snapshot}{srcInfo['dir']}"
            
            try:
                status = job_status[job][src_path_with_snapshot]['phases'][-1]['status']
            except:
                status = 'not started'                    

            # if status complete validate destination have the snapshot
            if status == 'complete':
                out = ssh(dstInfo['user']+'@'+dstInfo['host'],['volume','snapshot','show','-vserver',dstInfo['svm'],'-volume',dstInfo['vol'],'-snapshot',snapshot])   
                if not out['returncode']:
                    logging.info(f"job is completed for src: {src_path_with_snapshot} and snapshot: {snapshot} exists on dst: {dst}")
                else:
                    logging.error(f"job is completed for src: {src_path_with_snapshot} but snapshot: {snapshot} does not exists on dst: {dst}")
                    exit(1)

            #start baseline if prev job failed or not started
            if status in ['not started']:
                if snapcount ==  0:
                    logging.info(f"starting baseline for job: ndmpcopy from src:{src_path_with_snapshot} to dst:{dst}")
                    result = run_xcption_cmd(['baseline','-j',job,'-s','*'+src_path_with_snapshot])
                else:
                    logging.info(f"starting sync for job: ndmpcopy from src:{src_path_with_snapshot} to dst:{dst}")
                    result = run_xcption_cmd(['sync','-j',job,'-s','*'+src_path_with_snapshot])
                    result = run_xcption_cmd(['pause','-j',job,'-s','*'+src_path_with_snapshot])
                    result = run_xcption_cmd(['syncnow','-j',job,'-s','*'+src_path_with_snapshot])

                if result['returncode']:
                    logging.error(f"could not start job: {result['stderr']}")
                    exit(1)
            
                while True:
                    time.sleep(8)
                    job_status = get_xcption_status(job=job,src='*'+src_path_with_snapshot)[job][src_path_with_snapshot]
                    status = job_status['phases'][-1]['status']
                    logging.info(f"job status: {status}")

                    if status == 'failed':
                        logging.error(f"job failed:")
                        print(job_status['phases'][-1]['stdoutlogcontent'])
                        exit(1)

                    if status == 'complete':
                        #take snapshot of the destination
                        logging.info(f"creating snapshot: {snapshot} on the destination: {dstInfo['svm']}:{dstInfo['vol']}")
                        out = ssh(dstInfo['user']+'@'+dstInfo['host'],['volume','snapshot','create','-vserver',dstInfo['svm'],'-volume',dstInfo['vol'],'-snapshot',snapshot])   
                        if out['returncode']:
                            logging.error(f"could not take snapshot of the destination: {dst} error: {out['stderr']}")
                            exit(1) 
                        break                
            
            snapcount += 1

        


if __name__ == "__main__":
    main()
