job "{{ baseline_job_name }}" {
	datacenters = ["{{ dcname }}"]
	type = "batch"
	periodic {
		crons = ["0 0 31 2 *"]
		prohibit_overlap = true
	}
	constraint {
		attribute = "${attr.kernel.name}"
		value     = "{{ os }}"
	}

	group "{{ baseline_job_name }}" {
		count = 1
		reschedule {
			attempts = 0
		}
		restart {
			attempts = 0
			mode     = "fail"
		}
		task "baseline" {
			driver = "raw_exec"
      			kill_timeout = "120s"
			resources {
				cpu    = {{ cpu }}
				memory = {{ memory }}
			}
			logs {
				max_files     = 10
				max_file_size = 10
			}
			config {
				command = "{{ xcppath }}"
				args = ["{{ args }}"]
			}
		}
	}
}
