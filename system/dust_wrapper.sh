#!/bin/bash

export SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export PID=$$
export EXPORT=$1
export DEPTH=$2
export MINFILES=$3
export MAXTASKS=$4
export SRC=/tmp/dust_temp_path_${PID}
mkdir -p ${SRC}
mount -o vers=3 ${EXPORT} ${SRC}

stdbuf -i0 -o0 -e0 /usr/local/bin/dust -S 1073741824 --print-errors -x -D -p -j -r -f -n ${MAXTASKS} -d ${DEPTH} -X .snapshot -z ${MINFILES} ${SRC}
exitcode=$?

umount -l ${SRC}
if [ $? -eq 0 ]; then
	rm -rf ${SRC}
fi

exit $exitcode
