$srcpath = $args[0]
$dstpath = $args[1]
$basesrcpath = $args[2]
$basedstpath = $args[3]

$setacl = $true 

if (-not (test-path $srcpath)) {
    Write-Output "source path: $srcpath not found"
    exit 1
}
if (test-path $dstpath) { 
    $itemCount = (Get-ChildItem -Path $dstpath).Count
    if ($itemCount -gt 0) {
        Write-Output "WARNING: dest path: $dstpath already exists and not empty, ACL will not be configured on it as in the source"    
        $setacl = $false
    } else {
        Write-Output "dest path: $dstpath already exists"
    }

} else {
    Write-Output "creating dest path: $dstpath" 
    $create = New-Item -ItemType Directory -Path $dstpath -OutVariable out
    if (-not (test-path $dstpath)) {
        Write-Home "failed to create directory: $destpath"
        exit 1
    }
}

$parentsrcpath = $srcpath
$parentdstpath = $dstpath


while ($parentsrcpath -ne $basesrcpath) {
    Write-Output "setting dst path: $parentdstpath ACL and owner as the src path: $parentsrcpath"
    # Get the ACL from the source directory
    try {
        $acl = Get-Acl -Path $parentsrcpath
    } catch {
        Write-Output "Get-ACL for $parentsrcpath failed"
        exit 1
    }    
    # Set the ACL on the destination directory
    if ($setacl) {
        try {
            Set-Acl -Path $parentdstpath -AclObject $acl
        } catch {
            Write-Output "Set-ACL for $parentdstpath failed"
            exit 1
        }   
    }
    # Get the owner from the source directory
    try {
        $owner = (Get-Acl -Path $parentsrcpath).Owner
    } catch {
        Write-Output "Get owner for $parentsrcpath failed"
        exit 1
    }  
    # Set the owner on the destination directory
    if ($setacl) {
        try {
            $acl.SetOwner([System.Security.Principal.NTAccount]$owner)
            Set-Acl -Path $parentdstpath -AclObject $acl
        } catch {
            Write-Output "Set owner for $parentdstpath failed"
            exit 1
        }
    }
    $parentsrcpath = Split-Path -Path $parentsrcpath
    $parentdstpath = Split-Path -Path $parentdstpath
}
